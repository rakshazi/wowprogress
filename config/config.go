package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// Config is list of all guilds' configs
type Config []Item

// Item represents configuration set for 1 guild
type Item struct {
	ID           string
	Wowprogress  *Wowprogress  `json:"wowprogress,omitempty"`
	Warcraftlogs *Warcraftlogs `json:"warcraftlogs,omitempty"`
	Raiderio     *Raiderio     `json:"raiderio,omitempty"`
}

// Raiderio configuration
type Raiderio struct {
	Guild   string `json:"guild"`
	Members int    `json:"numMembers"`
	Realm   string `json:"realm"`
	RealmID int    `json:"realmId"`
	Region  string `json:"region"`
}

// Wowprogress configuration
type Wowprogress struct {
	Guild  string `json:"guild"`
	Realm  string `json:"realm"`
	Region string `json:"region"`
}

// Warcraftlogs configuration
type Warcraftlogs struct {
	GuildID int `json:"guildId"`
}

// CreateConfigurationFromFile Returns a new configuration loaded from a file
func CreateConfigurationFromFile(configFile string) (Config, error) {
	config := Config{}

	if _, err := os.Stat(configFile); !os.IsNotExist(err) {
		log.Printf("Found configuration file: %s\n", configFile)

		configData, err := ioutil.ReadFile(configFile)
		if err != nil {
			return config, fmt.Errorf("error reading configuation file: %v", err)
		}

		err = json.Unmarshal(configData, &config)
		if err != nil {
			return config, fmt.Errorf("error decoding configuration: %v", err)
		}
		// Prepare config values for WoW Progress
		for id, guildConfig := range config {
			switch {
			case guildConfig.Raiderio != nil:
				guildConfig.ID = "[" + guildConfig.Raiderio.Guild + "/" + guildConfig.Raiderio.Realm + "/" + guildConfig.Raiderio.Region + "]"
			case guildConfig.Wowprogress != nil:
				guildConfig.ID = "[" + guildConfig.Wowprogress.Guild + "/" + guildConfig.Wowprogress.Realm + "/" + guildConfig.Wowprogress.Region + "]"
				guildConfig.Wowprogress.Guild = strings.ReplaceAll(guildConfig.Wowprogress.Guild, " ", "+")
				guildConfig.Wowprogress.Realm = strings.ReplaceAll(guildConfig.Wowprogress.Realm, " ", "-")
				guildConfig.Wowprogress.Realm = strings.ToLower(guildConfig.Wowprogress.Realm)
			}
			config[id] = guildConfig
		}

		log.Print("Configuration loaded successfully\n")
	}
	return config, nil
}
