# [WGPU website](https://wgt.rakshazi.me) [![GoDoc](https://godoc.org/gitlab.com/rakshazi/wowprogress?status.svg)](https://godoc.org/gitlab.com/rakshazi/wowprogress)

This program will update your World of Warcraft guild  and characters progress on Raider.io, Warcraft Logs and Wowprogress.

Full update (Raider.io, Warcraft Logs, Wowprogress):

```bash
└──● time wowprogress
2020/02/04 11:03:52 Found configuration file: config.json
2020/02/04 11:03:52 Configuration loaded successfully
2020/02/04 11:03:53 [Совет Тирисфаля/Howling Fjord/eu] WowProgress successfully updated
2020/02/04 11:03:53 [Совет Тирисфаля/Howling Fjord/eu] RaiderIO successfully updated
2020/02/04 11:03:59 [Совет Тирисфаля/Howling Fjord/eu] WarcraftLogs successfully updated
2020/02/04 11:03:59 All resources updated

real    0m6.802s
user    0m0.179s
sys     0m0.030s
```

## Configuration

This tool needs a `config.json` file with following structure:

```json
[
    {
        "wowprogress": {
            "guild": "Совет Тирисфаля",
            "realm": "Ревущий Фьорд",
            "region": "eu"
        },
        "raiderio": {
            "guild": "Совет Тирисфаля",
            "members": 0,
            "realm": "Howling Fjord",
            "realmId": 624,
            "region": "eu"
        },
        "warcraftlogs": {
            "guildId": 253875
        }
    }
]
```

> **note**: if you don't need any of these resources, just remove it from config.

You can also specify the config file path with the `-config` flag. For example:

```bash
wowprogress -config my/custom/dir/my_config.json
```

Check more configuration options in the [conf.json.dist](config.json.dist)

## Run

### Golang

```bash
go get gitlab.com/rakshazi/wowprogress
cd $GOPATH/src/gitlab.com/rakshazi/wowprogress
go install
curl https://gitlab.com/rakshazi/wowprogress/raw/master/config.json.dist -o config.json
wowprogress
```

### Binary

```bash
# Get binary from project -> releases page

curl https://gitlab.com/rakshazi/wowprogress/raw/master/config.json.dist -o config.json
wowprogress
```

### Docker

```bash
curl https://gitlab.com/rakshazi/wowprogress/raw/master/config.json.dist -o config.json
docker run -d -v "$PWD/config.json:/etc/config.json" registry.gitlab.com/rakshazi/wowprogress
```

