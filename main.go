package main

import (
	"flag"
	"log"
	"sync"

	"gitlab.com/rakshazi/wowprogress/config"
	"gitlab.com/rakshazi/wowprogress/updater"
)

func main() {
	configFile := flag.String("config", "config.json", "Path to the configuration file")
	flag.Parse()
	configuration, err := config.CreateConfigurationFromFile(*configFile)
	if err != nil {
		log.Fatal(err)
	}
	update(configuration)
}

// Run updater
func update(configuration config.Config) {
	var wg sync.WaitGroup
	for _, guild := range configuration {
		wg.Add(1)
		go updater.Run(guild, &wg)
	}
	wg.Wait()
	log.Println("All resources updated")
}
