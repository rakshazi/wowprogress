package updater

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	configPkg "gitlab.com/rakshazi/wowprogress/config"
)

var wg sync.WaitGroup

//Run updater with provied configuration
func Run(config configPkg.Item, globalWg *sync.WaitGroup) {
	if config.Wowprogress != nil {
		wg.Add(1)
		go wowprogress(config.ID, config.Wowprogress, &wg)
	}
	if config.Warcraftlogs != nil {
		wg.Add(1)
		go warcraftlogs(config.ID, config.Warcraftlogs, &wg)
	}
	if config.Raiderio != nil {
		wg.Add(1)
		go raiderio(config.ID, config.Raiderio, &wg)
	}

	wg.Wait()
	globalWg.Done()
}

//Update Raider.io guild progress
func raiderio(id string, config *configPkg.Raiderio, wg *sync.WaitGroup) {
	url := "https://raider.io/api/crawler/guilds"
	jsonConfig, err := json.Marshal(config)
	if err != nil {
		log.Println(id, "Error marshaling RaiderIO config to JSON", err)
		wg.Done()
		return
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonConfig))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(id, "RaiderIO returned error", err)
		wg.Done()
		return
	}
	if isSuccess(resp) {
		log.Println(id, "RaiderIO successfully updated")
		wg.Done()
		return
	}

	log.Println(id, "RaiderIO update failed")
	wg.Done()
	return
}

//Update WarcraftLogs.com guild progress
func warcraftlogs(id string, config *configPkg.Warcraftlogs, wg *sync.WaitGroup) {
	url := "https://www.warcraftlogs.com/guild/update/" + strconv.Itoa(config.GuildID)

	req, err := http.NewRequest("GET", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(id, "WarcraftLogs returned error", err)
		wg.Done()
		return
	}
	if isSuccess(resp) {
		log.Println(id, "WarcraftLogs successfully updated")
		wg.Done()
		return
	}

	log.Println(id, "WarcraftLogs update failed")
	wg.Done()
	return
}

//Update Wowprogress.com guild progress
func wowprogress(id string, config *configPkg.Wowprogress, wg *sync.WaitGroup) {
	urlString := "https://www.wowprogress.com/update_progress/guild/" + config.Region + "/" + config.Realm + "/" + config.Guild
	data := wowprogressGetRequest(urlString)

	req, err := http.NewRequest("POST", urlString, strings.NewReader(data.Encode()))
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Set("Referer", urlString)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(id, "WowProgress returned error", err)
		wg.Done()
		return
	}

	if isSuccess(resp) {
		log.Println(id, "WowProgress successfully updated")
		wg.Done()
		return
	}

	log.Println(id, "WowProgress update failed")
	wg.Done()
	return
}

// Prepare Wowprogress Request body - grab characters ids
// from update page and return as url.Values object to use
// in http.Request
func wowprogressGetRequest(urlString string) url.Values {
	data := url.Values{}
	data.Set("submit", "1")
	doc, err := goquery.NewDocument(urlString)
	if err != nil {
		log.Fatal(err)
	}
	doc.Find("input.char_chbx").Each(func(index int, item *goquery.Selection) {
		id, _ := item.Attr("id")
		id = strings.ReplaceAll(id, "check_", "")
		data.Add("char_ids", string(id))
	})
	return data
}

// Check if request succeed
func isSuccess(resp *http.Response) bool {
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false
	}

	result := make(map[string]interface{})
	err = json.Unmarshal(bodyBytes, &result)
	if err != nil {
		return false
	}
	if resp.StatusCode != 200 || result["success"] != true {
		return false
	}

	return true
}
